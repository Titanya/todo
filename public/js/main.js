var todo = angular.module('todo', []);

function mainController($scope, $http, $timeout) {
	$scope.formData = {};
	$scope.active = 0;
	$scope.done = 0;;

	// when landing on the page, get all todos and show them
	$http.get('/api/todos')
		.success(function(data) {
			$scope.todos = data;
			calc(data);
		})
		.error(function(data) {
			console.log('Error: ' + data);
		});

	// when submitting the add form, send the text to the node API
	$scope.createTodo = function() {
		$http.post('/api/todos', $scope.formData)
			.success(function(data) {
				$scope.formData = {}; // clear the form so our user is ready to enter another
				$scope.todos = data;
				calc(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// delete a todo after checking it
	$scope.deleteTodo = function(id) {
		$http.delete('/api/todos/' + id)
			.success(function(data) {
				$scope.todos = data;
				calc(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	// update a todo
	$scope.updateTodo = function(id,status) {
		$http.put('/api/todos/' + id, { "done" :status})
			.success(function(data) {
				  $timeout(function () {
                    $scope.todos = data; 
                    calc(data);
                  }, 800);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};

	$scope.deleteAll = function() {
		$http.delete('/api/todos/')
			.success(function(data) {
				$scope.todos = data;
				calc(data);
			})
			.error(function(data) {
				console.log('Error: ' + data);
			});
	};
	function calc(data){
		    $scope.done = 0;
		    $scope.active = 0;
			var i = 0;
			for(i; i< data.length; i++){
               if(data[i].done == true){
                   $scope.done ++;
               }else{
               	 $scope.active ++;
               }
			}
	}
}