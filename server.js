// modules =================================================
var express        = require('express');
var app            = express();
var mongoose       = require('mongoose');
var bodyParser     = require('body-parser');
var methodOverride = require('method-override');

// configuration ===========================================
/*
** connect to test DB in MongoLAB via mongoose
** these credentials are mine put for the puropse of the demo
comment them and enter your own credentials after creating
an account in mongoLab
*/
mongoose.connect('mongodb://Juda:jobou106@ds253840.mlab.com:53840/tododb', function (error) {
    if (error) console.error(error);
    else console.log('connected with mongoLab');
});

var port = process.env.PORT || 8080; // set our port

// get all data/stuff of the body (POST) parameters
app.use(bodyParser.json()); // parse application/json 
app.use(bodyParser.json({ type: 'application/vnd.api+json' })); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({ extended: true })); // parse application/x-www-form-urlencoded

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

// routes ==================================================
require('./app/routes')(app); // pass our application into our routes

// start app ===============================================
app.listen(port);	
console.log('Listen on port ' + port); 			// shoutout to the user
exports = module.exports = app; 						// expose app